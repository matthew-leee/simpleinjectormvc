﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Contract.Interfaces;
using Microsoft.AspNetCore.Mvc;
using SimpleInjectorMVC.Models;
using TVDMS.Common.Commands;
using BLL.Contract.Models.Order;
using BLL.Contract.Commands.OrderCommands;
using TVDMS.Common.Queries;
using BLL.Contract.Queries.OrderQueries;
using TVDMS.Common.DI;

namespace SimpleInjectorMVC.Controllers
{
    public class OrderController : Controller
    {
        private ICommandBus commandBus;
        private IQueryProcessor queryProcessor;
        public OrderController(ICommandBus commandBus, IQueryProcessor queryProcessor)
        {
            this.commandBus = commandBus;
            this.queryProcessor = queryProcessor;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult CreateOrder()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CreateOrder(CreateOrder createOrder)
        {
            var createOrderCommand = new CreateOrderCommand();
            createOrder.Datetime = DateTime.Now;
            createOrderCommand.CreateOrder = createOrder;
            commandBus.Submit(createOrderCommand);
            return RedirectToAction("ViewOrder");
        }
        [HttpGet]
        public IActionResult ViewOrder(FilterOrder filterOrder, SortOrder sortOrder)
        {
            var viewOrderQuery = new ViewOrderQuery(filterOrder, sortOrder);
            var viewOrderList = queryProcessor.Process(viewOrderQuery);
            return View(viewOrderList);
        }
        public IActionResult ErrorOrder()
        {
            throw new Exception("error order");
        }
    }
}