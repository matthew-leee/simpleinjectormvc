﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace SimpleInjectorMVC.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Index(string e)
        {
            ViewData["error"] = e;
            return View();
        }
    }
}