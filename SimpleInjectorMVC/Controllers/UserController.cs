﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Contract.Commands.UserCommands;
using BLL.Contract.Models.User;
using BLL.Contract.Queries.UserQueries;
using Microsoft.AspNetCore.Mvc;
using TVDMS.Common.Commands;
using TVDMS.Common.Queries;

namespace SimpleInjectorMVC.Controllers
{
    public class UserController : Controller
    {
        private readonly ICommandBus commandBus;
        private readonly IQueryProcessor queryProcessor;
        public UserController(ICommandBus commandBus, IQueryProcessor queryProcessor)
        {
            this.commandBus = commandBus;
            this.queryProcessor = queryProcessor;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult LoginUser()
        {
            return View();
        }
        [HttpPost]
        public IActionResult LoginUser(LoginUser loginUser)
        {
            var loginUserSingleQuery = new LoginUserSingleQuery(loginUser);
            queryProcessor.ProcessSingleQuery(loginUserSingleQuery);
            ViewData["Login"] = "Login Success";
            return View();
        }
        [HttpGet]
        public IActionResult RegisterUser()
        {
            return View();
        }
        [HttpPost]
        public IActionResult RegisterUser(RegisterUser registerUser)
        {
            registerUser.Datetime = DateTime.Now;
            var registerUserCommand = new RegisterUserCommand(registerUser);
            commandBus.Submit(registerUserCommand);
            return RedirectToAction("LoginUser");
        }
    }
}