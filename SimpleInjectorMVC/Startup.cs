﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using BLL.Contract.Interfaces.IRepositories;
using BLL.Handlers.CommandHandlers.OrderCommandHandlers;
using BLL.Handlers.QueryHandlers.OrderQueryHandlers;
using BLL.Decorators.Sorters.OrderSorters;
using BLL.Decorators.Validators.OrderValidators;
using DAL.Repositories.OrderRepositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SimpleInjector;
using TVDMS.Common.Commands;
using TVDMS.Common.Commands.Decorators;
using TVDMS.Common.Commands.Validators;
using TVDMS.Common.DI;
using TVDMS.Common.Queries;
using TVDMS.Common.Queries.Decorators;
using BLL.Decorators.Validators.UserValidators;
using DAL.Repositories.UserRepositories;
using BLL.Handlers.CommandHandlers.UserCommandHandlers;
using TVDMS.Common.Commands.Encryptors;
using BLL.Decorators.Encryptors.UserEncryptors;
using BLL.Handlers.QueryHandlers.UserQueryHandlers;
using Microsoft.AspNetCore.Diagnostics;
using SharedUtil;
using BLL.Handlers.SingleQueryHandlers;
using BLL.Contract.Interfaces.IMiddlewares;
using SimpleInjectorMVC.Middlewares;
using SimpleInjectorMVC.Middlewares.MiddlewareExtensions;

namespace SimpleInjectorMVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private Container container = new Container();

        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddSessionStateTempDataProvider();

            services.AddSession();

            services.AddSimpleInjector(container, options =>
            {
                // AddAspNetCore() wraps web requests in a Simple Injector scope.
                options.AddAspNetCore()
                    // Ensure activation of a specific framework type to be created by
                    // Simple Injector instead of the built-in configuration system.
                    .AddControllerActivation()
                    .AddViewComponentActivation()
                    .AddPageModelActivation()
                    .AddTagHelperActivation();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCustomStatusCodeHandler();

            if (env.IsDevelopment())
            {
                // custom middleware
                app.UseCustomGlobalExceptionHandler();
            }
            else
            {
                // custom middleware
                app.UseCustomGlobalExceptionHandler();

                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCustomRequestHandler();
            
            app.UseHttpsRedirection();
            // app.UseStatusCodePagesWithReExecute("/Error/Index", "?e={0}");
            app.UseStaticFiles();
            app.UseCookiePolicy();
            
            // UseSimpleInjector() enables framework services to be injected into
            // application components, resolved by Simple Injector.
            app.UseSimpleInjector(container, options =>
            {
                // Add custom Simple Injector-created middleware to the ASP.NET pipeline.
                // options.UseMiddleware<CustomMiddleware1>(app);
                // options.UseMiddleware<CustomMiddleware2>(app);

                // Optionally, allow application components to depend on the
                // non-generic Microsoft.Extensions.Logging.ILogger abstraction.
                options.UseLogging();
            });

            DIContainer.Instance = new SimpleDIContainer(container);

            InitializeContainer();

            // Always verify the container
            container.Verify();

            app.UseSession();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void InitializeContainer()
        {
            var validatorAssemblies = new[] { typeof(IValidator<>).Assembly };
            container.Collection.Register(typeof(IValidator<>), validatorAssemblies);
            container.Collection.Append(typeof(IValidator<>), typeof(CreateOrderFoodValidator));
            container.Collection.Append(typeof(IValidator<>), typeof(CreateOrderNameValidator));
            container.Collection.Append(typeof(IValidator<>), typeof(FilterOrderNameValidator));
            container.Collection.Append(typeof(IValidator<>), typeof(FilterOrderFoodValidator));
            container.Collection.Append(typeof(IValidator<>), typeof(FilterOrderPriceValidator));

            container.Collection.Append(typeof(IValidator<>), typeof(RegisterUserPasswordValidator));
            container.Collection.Append(typeof(IValidator<>), typeof(RegisterUserUsernameEmailValidator));

            container.Collection.Append(typeof(IValidator<>), typeof(LoginUserEmailValidator));
            container.Collection.Append(typeof(IValidator<>), typeof(LoginUserPasswordValidator));
            container.Collection.Append(typeof(IValidator<>), typeof(LoginUserUsernameValidator));

            var queryPostSingleQueryValidatorsAssemblies = new[] { typeof(IQueryPostSingleQueryValidator<,>).Assembly };
            container.Collection.Register(typeof(IQueryPostSingleQueryValidator<,>), queryPostSingleQueryValidatorsAssemblies);
            container.Collection.Append(typeof(IQueryPostSingleQueryValidator<,>), typeof(LoginUserPasswordPostSingleQueryValidator));

            container.Register<IOrderRepository, OrderRepository>();
            container.Register<IUserRepository, UserRepository>();

            var encryptorAssemblies = new[] { typeof(IEncryptor<>).Assembly };
            container.Collection.Register(typeof(IEncryptor<>), encryptorAssemblies);
            container.Collection.Append(typeof(IEncryptor<>), typeof(RegisterUserHashEncryptor));

            container.Register(typeof(ICommandBus), typeof(MemoryCommandBus));
            container.Register(typeof(ICommandHandler<>), typeof(CreateOrderCommandHandler));
            container.Register(typeof(ICommandHandler<>), typeof(RegisterUserCommandHandler));
            container.RegisterDecorator(typeof(ICommandHandler<>), typeof(ValidationCommandHandlerDecorator<>));
            container.RegisterDecorator(typeof(ICommandHandler<>), typeof(EncryptionCommandHandlerDecorator<>));

            container.Register<IQueryProcessor, QueryProcessor>();
            container.Register(typeof(IQueryHandler<,>), typeof(ViewOrderQueryHandler));
            container.RegisterDecorator(typeof(IQueryHandler<,>), typeof(ValidationQueryHandlerDecorator<,>));
            container.Register(typeof(IQueryHandler<,>), typeof(ValidateRegisterUserQueryHandler));
            container.Register(typeof(ISingleQueryHandler<,>), typeof(LoginUserSingleQueryHandler));

            var querySorterAssemblies = new[] { typeof(IQuerySorter<,>).Assembly };
            container.Collection.Register(typeof(IQuerySorter<,>), querySorterAssemblies);
            container.Collection.Append(typeof(IQuerySorter<,>), typeof(AscendingViewOrderSorter));
            container.Collection.Append(typeof(IQuerySorter<,>), typeof(DescendingViewOrderSorter));
            container.RegisterDecorator(typeof(IQueryHandler<,>), typeof(SorterQueryHandlerDecorator<,>));

            
            container.RegisterDecorator(typeof(ISingleQueryHandler<,>), typeof(PostSingleQueryValidationQueryHandlerDecorator<,>));

        }
    }
}
