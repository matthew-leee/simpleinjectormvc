﻿using BLL.Contract.Interfaces.IMiddlewares;
using Microsoft.AspNetCore.Builder;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleInjectorMVC.Middlewares.MiddlewareExtensions
{
    public static class CustomGlobalErrorHandlingMiddlewareExtension
    {
        public static IApplicationBuilder UseCustomGlobalExceptionHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomGlobalExceptionHandlingMiddleware>();
        }

        public static IApplicationBuilder UseCustomStatusCodeHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomStatusCodeHandlingMiddleware>();
        }

        public static IApplicationBuilder UseCustomRequestHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomRequestHandlingMiddleware>();
        }
    }
}
