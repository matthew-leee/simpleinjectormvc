﻿using BLL.Contract.Interfaces.IMiddlewares;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Runtime.Serialization.Json;

namespace SimpleInjectorMVC.Middlewares
{
    public class CustomRequestHandlingMiddleware : ICustomMiddleware
    {
        private readonly RequestDelegate next;

        public CustomRequestHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                if (context.Request.ContentType == "application/json")
                {
                    
                    using (var streamReader = new HttpRequestStreamReader(context.Request.Body, Encoding.UTF8))
                    {
                        var rawJson = await streamReader.ReadToEndAsync();
                        MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(rawJson));
                        DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(object));
                        var result = ser.ReadObject(ms);
                        //using (var jsonReader = new JsonTextReader(streamReader))
                        //{
                        //    var json = await JObject.LoadAsync(jsonReader);
                        //    var parsed = JObject.Parse(json.ToString());

                        //}
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Request middleware error");
            }
            await next(context);
            //var request = context.Request;
            //if (request.ContentType == "application/json")
            //{
            //    context.Response.Redirect("/Error/Index?e=requestedFetchVersion");
            //}
        }
    }
}
