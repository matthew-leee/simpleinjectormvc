﻿using BLL.Contract.Interfaces.IMiddlewares;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleInjectorMVC.Middlewares
{
    public class CustomStatusCodeHandlingMiddleware: ICustomMiddleware
    {
        private readonly RequestDelegate next;

        public CustomStatusCodeHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            await next(context);
            var statusCode = context.Response.StatusCode;
            if (statusCode == 404)
            {
                context.Response.Redirect("/Error/Index?e=404!!!!!!", false);
            }
        }
    }
}
