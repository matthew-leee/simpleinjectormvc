﻿using BLL.Contract.Interfaces.IMiddlewares;
using BLL.Contract.Models.Middlewares;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SharedUtil;
using SimpleInjectorMVC.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleInjectorMVC.Middlewares
{
    public class CustomGlobalExceptionHandlingMiddleware: ICustomMiddleware
    {
        private readonly RequestDelegate next;

        public CustomGlobalExceptionHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch(InvalidOperationException ex)
            {
                context.Response.Redirect($"/Error/Index?e={ex.Message}", false);
            }
            catch (Exception e)
            {
               var customProblemDetails = new CustomProblemDetails
                {
                    Title = "An unexpected error occurred!happy!",
                    StatusCode = context.Response.StatusCode,
                    Detail = e.Message,
                    Id = $"urn::error:{Guid.NewGuid()}",
                    dateTime = DateTime.Now
                };
                context.Response.WriteJson(customProblemDetails, "application/problem+json");
            }
        }
    }
}
