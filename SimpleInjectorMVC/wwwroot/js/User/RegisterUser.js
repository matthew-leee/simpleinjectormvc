﻿const FormElem = document.getElementById("RegisterUserForm");
const ErrorElem = document.querySelector("#RegisterUserErrorJS");
const FetchLocation = "/User/RegisterUser";
const FetchMethod = "post";
const WindowLocation = "/User/LoginUser";

document.querySelector("#RegisterUserBtn").addEventListener("click", async e => {
    await errorHandler(e, FormElem, FetchLocation, FetchMethod, ErrorElem, WindowLocation);
});