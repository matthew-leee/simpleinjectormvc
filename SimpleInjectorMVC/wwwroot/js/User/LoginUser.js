﻿const FormElem = document.querySelector("#LoginUserForm");
const ErrorElem = document.querySelector("#LoginUserStatusJS");
const FetchLocation = "/User/LoginUser";
const FetchMethod = "post";
const WindowLocation = "/User/LoginUser";

const btn = document.querySelector("#LoginUserBtn")

btn.addEventListener("click", async e => {
    await errorHandler(e, FormElem, FetchLocation, FetchMethod, ErrorElem, WindowLocation);
});