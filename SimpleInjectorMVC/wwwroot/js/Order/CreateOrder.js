﻿const FormElem = document.getElementById("CreateOrderForm");
const ErrorElem = document.querySelector("#createOrderErrorJS");
const FetchLocation = "/Order/CreateOrder";
const FetchMethod = "post";
const WindowLocation = "/Order/ViewOrder";

// document.querySelector("#CreateOrderBtn").addEventListener("click", async e => {
//     await errorHandler(e, FormElem, FetchLocation, FetchMethod, ErrorElem, WindowLocation);
// })

async function errorHandler(e, formElem, fetchLocation, fetchMethod, errorElem, windowLocation) {
    e.preventDefault();
    const form = new FormData(formElem);
    const json = {
        Name: "HardCodeName",
        Food: "HardCodeFood",
        Price: 500
    }
    const data = JSON.stringify(json)
    
    const res = await fetch(fetchLocation, {
        method: fetchMethod,
        body: data,
        headers: {
            'Content-Type': 'application/json'
        }
    })
    //if (res.status == 500) {
    //    const json = await res.json();
    //    console.log(json)
    //    errorElem.innerHTML = json.detail;
    //} else {
    //    // window.location.href = windowLocation;
    //    console.log("happy")
    //}
    console.log (res)
}


 document.querySelector("#CreateOrderBtn").addEventListener("click", async e => {
     await errorHandler(e, FormElem, FetchLocation, FetchMethod, ErrorElem, WindowLocation);
 })