﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using SharedUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleInjectorMVC
{
    public static class GlobalErrorHandling
    {
        public static void GlobalErrorHandler(this IApplicationBuilder errorApp)
        {
            errorApp.Run(async context =>
            {
                var errorFeature = context.Features.Get<IExceptionHandlerFeature>();
                var exception = errorFeature.Error;

                // the IsTrusted() extension method doesn't exist and
                // you should implement your own as you may want to interpret it differently
                // i.e. based on the current principal

                //var errorDetail = context.Request.IsTrusted()
                //    ? exception.Demystify().ToString()
                //    : "The instance value should be used to identify the problem when calling customer support";

                var problemDetails = new ProblemDetails
                {
                    Title = "An unexpected error occurred!",
                    Status = 500,
                    Detail = exception.Message,
                    Instance = $"urn:myorganization:error:{Guid.NewGuid()}"
                };

                context.Response.WriteJson(problemDetails, "application/problem+json");
                // log the exception etc..
                // flush problemDetails to the caller
            });
        }
    }
}
