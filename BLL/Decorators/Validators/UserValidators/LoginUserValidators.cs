﻿using BLL.Contract.Queries.UserQueries;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TVDMS.Common.Commands.Validators;

namespace BLL.Decorators.Validators.UserValidators
{
    public class LoginUserUsernameValidator: IValidator<LoginUserSingleQuery>
    {
        public void ValidateObject(LoginUserSingleQuery instance)
        {
            if (string.IsNullOrEmpty(instance.LoginUser.Username))
            {
                throw new ValidationException("Login User Username missing");
            }
        }
    }

    public class LoginUserEmailValidator : IValidator<LoginUserSingleQuery>
    {
        public void ValidateObject(LoginUserSingleQuery instance)
        {
            if (string.IsNullOrEmpty(instance.LoginUser.Email))
            {
                throw new ValidationException("Login User Email missing");
            }
        }
    }

    public class LoginUserPasswordValidator : IValidator<LoginUserSingleQuery>
    {
        public void ValidateObject(LoginUserSingleQuery instance)
        {
            if (string.IsNullOrEmpty(instance.LoginUser.Password))
            {
                throw new ValidationException("Login User Password missing");
            }
        }
    }
}
