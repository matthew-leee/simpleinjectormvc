﻿using BLL.Contract.Commands.UserCommands;
using BLL.Contract.Models.User;
using BLL.Contract.Queries.UserQueries;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Commands.Validators;
using TVDMS.Common.Queries;

namespace BLL.Decorators.Validators.UserValidators
{
    public class RegisterUserPasswordValidator : IValidator<RegisterUserCommand>
    {
        public void ValidateObject(RegisterUserCommand instance)
        {
            if (string.IsNullOrEmpty(instance.RegisterUser.Password))
            {
                throw new Exception("Password missing");
            }
        }
    }
    public class RegisterUserUsernameEmailValidator : IValidator<RegisterUserCommand>
    {
        private IQueryProcessor queryProcessor;
        public RegisterUserUsernameEmailValidator(IQueryProcessor queryProcessor)
        {
            this.queryProcessor = queryProcessor;
        }
        public void ValidateObject(RegisterUserCommand instance)
        {
            var validateRegisterUser = new ValidateRegisterUser(instance.RegisterUser.Username, instance.RegisterUser.Email);
            var validateRegisterUserQuery = new ValidateRegisterUserQuery(validateRegisterUser);
            var result = queryProcessor.Process(validateRegisterUserQuery);
            if (result.Count > 0)
            {
                throw new Exception("The Username or Email has been registered");
            }
        }
    }

}
