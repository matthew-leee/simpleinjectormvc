﻿using BLL.Contract.Models.User;
using BLL.Contract.Queries.UserQueries;
using Microsoft.AspNetCore.DataProtection;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TVDMS.Common.Queries.Decorators;

namespace BLL.Decorators.Validators.UserValidators
{
    public class LoginUserPasswordPostSingleQueryValidator : IQueryPostSingleQueryValidator<LoginUserSingleQuery, LoginUserPassword>
    {
        private IDataProtector protector;
        public LoginUserPasswordPostSingleQueryValidator(IDataProtectionProvider dataProtectionProvider)
        {
            protector = dataProtectionProvider.CreateProtector("ValidateRegisterUser");
        }
        public LoginUserPassword Validate(LoginUserSingleQuery query, LoginUserPassword instance)
        {
            var inputPassword = query.LoginUserInputPassword;
            var realPassword = protector.Unprotect(instance.Password);
            if (inputPassword != realPassword)
            {
                throw new ValidationException("Login User Wrong Password");
            }
            return instance;
        }
    }
}
