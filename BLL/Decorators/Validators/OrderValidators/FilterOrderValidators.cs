﻿using BLL.Contract.Queries.OrderQueries;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Commands.Validators;

namespace BLL.Decorators.Validators.OrderValidators
{
    public class FilterOrderNameValidator : IValidator<ViewOrderQuery>
    {
        public void ValidateObject(ViewOrderQuery instance)
        {
            if (instance.filterOrder.NameLength == null)
                instance.filterOrder.NameLength = 0;
        }
    }
    public class FilterOrderFoodValidator : IValidator<ViewOrderQuery>
    {
        public void ValidateObject(ViewOrderQuery instance)
        {
            if (instance.filterOrder.FoodLength == null)
                instance.filterOrder.FoodLength = 0;
        }
    }
    public class FilterOrderPriceValidator : IValidator<ViewOrderQuery>
    {
        public void ValidateObject(ViewOrderQuery instance)
        {
            if (instance.filterOrder.PriceRange == null)
                instance.filterOrder.PriceRange = 0;
        }
    }
}
