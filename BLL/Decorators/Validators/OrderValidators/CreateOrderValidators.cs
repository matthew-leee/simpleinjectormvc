﻿
using BLL.Contract.Commands.OrderCommands;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Text;
using TVDMS.Common.Commands;
using TVDMS.Common.Commands.Validators;

namespace BLL.Decorators.Validators.OrderValidators
{
    public class CreateOrderNameValidator : IValidator<CreateOrderCommand>
    {
        public void ValidateObject(CreateOrderCommand instance)
        {
            var createOrder = instance.CreateOrder;
            if (string.IsNullOrEmpty(createOrder.Name))
            {
                throw new Exception("Give me your name!");
            }
        }
    }

    public class CreateOrderFoodValidator : IValidator<CreateOrderCommand>
    {
        public void ValidateObject(CreateOrderCommand instance)
        {
            var createOrder = instance.CreateOrder;
            if (string.IsNullOrEmpty(createOrder.Food))
            {
                throw new InvalidOperationException("error page test");
            }
        }
    }
}
