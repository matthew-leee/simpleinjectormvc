﻿using BLL.Contract.Models.Order;
using BLL.Contract.Queries.OrderQueries;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Queries.Decorators;
using System.Linq;

namespace BLL.Decorators.Sorters.OrderSorters
{
    public class AscendingViewOrderSorter : IQuerySorter<ViewOrderQuery, ViewOrder>
    {
        public IList<ViewOrder> Sort(ViewOrderQuery query, IList<ViewOrder> instance)
        {
            if (query.sortOrder.SortMethod == "ascending")
            {
                switch (query.sortOrder.SortColumn)
                {
                    case "Name":
                        return instance.OrderBy(x => x.Name.Length).ToList();
                    case "Food":
                        return instance.OrderBy(x => x.Food.Length).ToList();
                    case "Price":
                        return instance.OrderBy(x => x.Price).ToList();
                    case "Datetime":
                        return instance.OrderBy(x => x.Datetime).ToList();
                    default:
                        throw new Exception("ascending sorting error");
                }
            }
            return instance;
        }
    }
    public class DescendingViewOrderSorter : IQuerySorter<ViewOrderQuery, ViewOrder>
    {
        public IList<ViewOrder> Sort(ViewOrderQuery query, IList<ViewOrder> instance)
        {
            if (query.sortOrder.SortMethod == "descending")
            {
                switch (query.sortOrder.SortColumn)
                {
                    case "Name":
                        return instance.OrderByDescending(x => x.Name.Length).ToList();
                    case "Food":
                        return instance.OrderByDescending(x => x.Food.Length).ToList();
                    case "Price":
                        return instance.OrderByDescending(x => x.Price).ToList();
                    case "Datetime":
                        return instance.OrderByDescending(x => x.Datetime).ToList();
                    default:
                        throw new Exception("descending sorting error");
                }
            }
            return instance;
        }
    }
}
