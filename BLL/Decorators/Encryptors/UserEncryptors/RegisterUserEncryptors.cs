﻿using BLL.Contract.Commands.UserCommands;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Commands.Encryptors;
using System.Security.Cryptography;
using Microsoft.AspNetCore.DataProtection;

namespace BLL.Decorators.Encryptors.UserEncryptors
{
    public class RegisterUserHashEncryptor : IEncryptor<RegisterUserCommand>
    {

        //private const int SaltSize = 32;
        //private const int HashSize = 20;
        //private const string HashString = "RegisterUserHash";
        private IDataProtector protector;
        public RegisterUserHashEncryptor(IDataProtectionProvider dataProtectionProvider)
        {
            protector = dataProtectionProvider.CreateProtector("ValidateRegisterUser");
        }
        private string Hash(string Password)
        {
            //byte[] salt = new byte[SaltSize];
            //using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
            //{
            //    rngCsp.GetBytes(salt);
            //}
            //var pbkdf2 = new Rfc2898DeriveBytes(Password, salt);
            //var hash = pbkdf2.GetBytes(HashSize);

            //var hashBytes = new byte[SaltSize + HashSize];
            //Array.Copy(salt, 0, hashBytes, 0, SaltSize);
            //Array.Copy(hash, 0, hashBytes, SaltSize, HashSize);

            //return $"{HashString}{Convert.ToBase64String(hashBytes)}";
            return protector.Protect(Password);
        }
        public RegisterUserCommand Encrypt(RegisterUserCommand command)
        {
            command.RegisterUser.EncryptedPassword = string.IsNullOrEmpty(command.RegisterUser.EncryptedPassword)
                ? Hash(command.RegisterUser.Password)
                : Hash(command.RegisterUser.EncryptedPassword);
            return command;
        }
    }
}
