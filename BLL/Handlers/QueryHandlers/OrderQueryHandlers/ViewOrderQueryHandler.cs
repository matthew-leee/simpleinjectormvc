﻿using BLL.Contract.Interfaces.IRepositories;
using BLL.Contract.Models.Order;
using BLL.Contract.Queries.OrderQueries;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Queries;

namespace BLL.Handlers.QueryHandlers.OrderQueryHandlers
{
    public class ViewOrderQueryHandler : IQueryHandler<ViewOrderQuery, ViewOrder>
    {
        private IOrderRepository orderRepository;
        public ViewOrderQueryHandler(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }
        public IList<ViewOrder> Handle(ViewOrderQuery query)
        {

            return orderRepository.ViewOrder(query);
        }
    }
}
