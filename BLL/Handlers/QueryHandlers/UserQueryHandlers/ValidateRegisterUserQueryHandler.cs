﻿using BLL.Contract.Interfaces.IRepositories;
using BLL.Contract.Models.User;
using BLL.Contract.Queries.UserQueries;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Queries;

namespace BLL.Handlers.QueryHandlers.UserQueryHandlers
{
    public class ValidateRegisterUserQueryHandler : IQueryHandler<ValidateRegisterUserQuery, ValidateRegisterUser>
    {
        private IUserRepository userRepository;
        public ValidateRegisterUserQueryHandler(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }
        public IList<ValidateRegisterUser> Handle(ValidateRegisterUserQuery query)
        {
            return userRepository.ValidateRegisterUser(query);
        }
    }
}
