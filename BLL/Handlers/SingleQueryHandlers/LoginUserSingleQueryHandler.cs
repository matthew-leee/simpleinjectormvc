﻿using BLL.Contract.Interfaces.IRepositories;
using BLL.Contract.Models.User;
using BLL.Contract.Queries.UserQueries;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Queries;

namespace BLL.Handlers.SingleQueryHandlers
{
    public class LoginUserSingleQueryHandler : ISingleQueryHandler<LoginUserSingleQuery, LoginUserPassword>
    {
        private IUserRepository userRepository;
        public LoginUserSingleQueryHandler(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }
        public LoginUserPassword Handle(LoginUserSingleQuery query)
        {
            return userRepository.LoginUser(query);
        }
    }
}
