﻿using BLL.Contract.Commands.OrderCommands;
using BLL.Contract.Interfaces.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Commands;

namespace BLL.Handlers.CommandHandlers.OrderCommandHandlers
{
    public class CreateOrderCommandHandler : ICommandHandler<CreateOrderCommand>
    {
        private IOrderRepository orderRepository;
        public CreateOrderCommandHandler(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }

        public void Handle(CreateOrderCommand command)
        {
            orderRepository.CreateOrder(command);
        }
    }
}
