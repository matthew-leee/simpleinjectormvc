﻿using BLL.Contract.Commands.UserCommands;
using BLL.Contract.Interfaces.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TVDMS.Common.Commands;

namespace BLL.Handlers.CommandHandlers.UserCommandHandlers
{
    public class AsyncRegisterUserCommandHandler : IAsyncCommandHandler<RegisterUserCommand>
    {
        private IAsyncUserRepository asyncUserRepository;
        public AsyncRegisterUserCommandHandler(IAsyncUserRepository asyncUserRepository)
        {
            this.asyncUserRepository = asyncUserRepository;
        }
        public async Task Handle(RegisterUserCommand command)
        {
            await asyncUserRepository.RegisterUser(command);
        }
    }
}
