﻿using BLL.Contract.Commands.UserCommands;
using BLL.Contract.Interfaces.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Commands;

namespace BLL.Handlers.CommandHandlers.UserCommandHandlers
{
    public class RegisterUserCommandHandler : ICommandHandler<RegisterUserCommand>
    {
        private IUserRepository userRepository;
        public RegisterUserCommandHandler(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }
        public void Handle(RegisterUserCommand command)
        {
            userRepository.RegisterUser(command);
        }
    }
}
