﻿using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Commands;

namespace DAL.Entities.OrderEntities
{
    public class CreateOrderEntity: ICommand
    {
        public string Name { get; set; }
        public string Food { get; set; }
        public int Price { get; set; }
        public DateTime Datetime { get; set; }
    }
}
