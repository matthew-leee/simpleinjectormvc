﻿using BLL.Contract.Commands.UserCommands;
using BLL.Contract.Interfaces.IRepositories;
using BLL.Contract.Models.User;
using BLL.Contract.Queries.UserQueries;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.UserRepositories
{
    public class AsyncUserRepository : IAsyncUserRepository
    {
        private IConfiguration Configuration { get; }

        public AsyncUserRepository(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private SqlConnection GetSqlConnection()
        {
            return new SqlConnection(Configuration["ConnectionStrings:Order"]);
        }

        public async Task RegisterUser(RegisterUserCommand registerUserCommand)
        {
            var registerUser = registerUserCommand.RegisterUser;
            using (SqlConnection connection = GetSqlConnection())
            {
                try
                {
                    await connection.OpenAsync();
                    using (SqlCommand sqlcommand = new SqlCommand("RegisterUser", connection))
                    {
                        sqlcommand.CommandType = CommandType.StoredProcedure;
                        sqlcommand.Parameters.Add("@Username", SqlDbType.VarChar).Value = registerUser.Username;
                        sqlcommand.Parameters.Add("@Email", SqlDbType.VarChar).Value = registerUser.Email;
                        sqlcommand.Parameters.Add("@Password", SqlDbType.VarChar).Value = registerUser.EncryptedPassword;
                        sqlcommand.Parameters.Add("@Datetime", SqlDbType.DateTime).Value = registerUser.Datetime;
                        await sqlcommand.ExecuteNonQueryAsync();
                    }
                    connection.Close();
                }
                catch
                {
                    throw new Exception("Register User Repo error");
                }
            }
        }
        public async Task<IList<ValidateRegisterUser>> ValidateRegisterUser(ValidateRegisterUserQuery validateRegisterUserQuery)
        {
            var validateRegisterUserList = new List<ValidateRegisterUser>();
            using (SqlConnection connection = GetSqlConnection())
            {
                try
                {
                    await connection.OpenAsync();
                    using (SqlCommand sqlcommand = new SqlCommand("ValidateRegisterUser", connection))
                    {
                        sqlcommand.CommandType = CommandType.StoredProcedure;
                        sqlcommand.Parameters.Add("@Username", SqlDbType.VarChar).Value = validateRegisterUserQuery.ValidateRegisterUser.Username;
                        sqlcommand.Parameters.Add("@Email", SqlDbType.VarChar).Value = validateRegisterUserQuery.ValidateRegisterUser.Email;
                        using (SqlDataReader dataReader = await sqlcommand.ExecuteReaderAsync())
                        {
                            while (dataReader.Read())
                            {
                                var validateRegisterUser = new ValidateRegisterUser
                                    (
                                        Convert.ToString(dataReader["Username"]),
                                        Convert.ToString(dataReader["Email"])
                                    );
                                validateRegisterUserList.Add(validateRegisterUser);
                            }
                        }
                    }
                    connection.Close();
                }
                catch
                {
                    throw new Exception("Validate Register User Error");
                }
            }
            return validateRegisterUserList;
        }
        public async Task<LoginUserPassword> LoginUser(LoginUserSingleQuery query)
        {
            var loginUserPassword = new LoginUserPassword();
            using (SqlConnection connection = GetSqlConnection())
            {
                try
                {
                    await connection.OpenAsync();
                    using (SqlCommand sqlcommand = new SqlCommand("LoginUser", connection))
                    {
                        sqlcommand.CommandType = CommandType.StoredProcedure;
                        sqlcommand.Parameters.Add("@Username", SqlDbType.VarChar).Value = query.LoginUser.Username;
                        sqlcommand.Parameters.Add("@Email", SqlDbType.VarChar).Value = query.LoginUser.Email;
                        using (SqlDataReader dataReader = await sqlcommand.ExecuteReaderAsync())
                        {
                            while (dataReader.Read())
                            {
                                loginUserPassword.Password = Convert.ToString(dataReader["Password"]);
                            }
                        }
                    }
                    connection.Close();
                }
                catch
                {
                    throw new Exception("Login User Repo Error");
                }
            }
            return loginUserPassword;
        }
    }

}
