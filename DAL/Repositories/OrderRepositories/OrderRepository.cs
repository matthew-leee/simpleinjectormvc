﻿using BLL.Contract.Commands.OrderCommands;
using BLL.Contract.Interfaces.IRepositories;
using BLL.Contract.Models.Order;
using BLL.Contract.Queries.OrderQueries;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Repositories.OrderRepositories
{
    public class OrderRepository : IOrderRepository
    {
        private IConfiguration Configuration { get; }

        public OrderRepository(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private SqlConnection GetSqlConnection()
        {
            return new SqlConnection(Configuration["ConnectionStrings:Order"]);
        }
        public void CreateOrder(CreateOrderCommand command)
        {
            var createOrder = command.CreateOrder;
            using (SqlConnection connection = GetSqlConnection())
            {
                try
                {
                    connection.Open();
                    using (SqlCommand sqlcommand = new SqlCommand("CreateOrder", connection))
                    {
                        sqlcommand.CommandType = CommandType.StoredProcedure;
                        sqlcommand.Parameters.Add("@Name", SqlDbType.VarChar).Value = createOrder.Name;
                        sqlcommand.Parameters.Add("@Food", SqlDbType.VarChar).Value = createOrder.Food;
                        sqlcommand.Parameters.Add("@Price", SqlDbType.Int).Value = createOrder.Price;
                        sqlcommand.Parameters.Add("@Datetime", SqlDbType.DateTime).Value = createOrder.Datetime;
                        sqlcommand.ExecuteNonQuery();
                    }
                    connection.Close();
                }
                catch
                {
                    throw new Exception("Create Order Handler error");
                }
            }
        }
        public List<ViewOrder> ViewOrder(ViewOrderQuery viewOrderQuery)
        {
            var viewOrderList = new List<ViewOrder>();

            using (SqlConnection connection = GetSqlConnection())
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("ViewOrder", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@NameFilter", SqlDbType.Int).Value = viewOrderQuery.filterOrder.NameLength;
                    command.Parameters.Add("@FoodFilter", SqlDbType.Int).Value = viewOrderQuery.filterOrder.FoodLength;
                    command.Parameters.Add("@PriceFilter", SqlDbType.Int).Value = viewOrderQuery.filterOrder.PriceRange;
                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var viewOrder = new ViewOrder
                            {
                                Id = Convert.ToInt32(dataReader["Id"]),
                                Name = Convert.ToString(dataReader["Name"]),
                                Food = Convert.ToString(dataReader["Food"]),
                                Price = Convert.ToInt32(dataReader["Price"]),
                                Datetime = Convert.ToDateTime(dataReader["Datetime"])
                            };
                            viewOrderList.Add(viewOrder);
                        }
                    }
                }
                connection.Close();
            }
            return viewOrderList;
        }
    }
}
