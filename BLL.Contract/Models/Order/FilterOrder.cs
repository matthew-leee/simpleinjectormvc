﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Contract.Models.Order
{
    public class FilterOrder
    {
        public int? NameLength { get; set; }
        public int? FoodLength { get; set; }
        public int? PriceRange { get; set; }
    }
}
