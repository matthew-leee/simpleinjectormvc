﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Contract.Models.Order
{
    
    public class SortOrder
    {
        public string SortColumn { get; set; }
        public string SortMethod { get; set; }
    }
}
