﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Contract.Models.Order
{
    public class ViewOrder
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Food { get; set; }
        public int Price { get; set; }
        public DateTime Datetime { get; set; }
    }
}
