﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Contract.Models.Middlewares
{
    public class CustomProblemDetails
    {
        public string Title { get; set; }
        public int StatusCode { get; set; }
        public string Detail { get; set; }
        public string Id { get; set; }
        public DateTime dateTime { get; set; }
    }
}
