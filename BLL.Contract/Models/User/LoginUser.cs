﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Contract.Models.User
{
    public class LoginUser
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
