﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Contract.Models.User
{
    public class RegisterUser
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string EncryptedPassword { get; set; }
        public DateTime Datetime { get; set; }
    }
}
