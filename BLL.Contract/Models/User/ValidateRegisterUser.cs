﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Contract.Models.User
{
    public class ValidateRegisterUser
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public ValidateRegisterUser(string username, string email)
        {
            Username = username;
            Email = email;
        }
    }
}
