﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Contract.Interfaces
{
    public interface IUserDataManager
    {
        void SetUserData(string userDataKey, string data);
        string GetUserData(string userDataKey);
    }
}
