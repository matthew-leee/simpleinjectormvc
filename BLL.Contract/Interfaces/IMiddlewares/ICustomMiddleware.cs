﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Contract.Interfaces.IMiddlewares
{
    public interface ICustomMiddleware
    {
        Task InvokeAsync(HttpContext context);
    }
}
