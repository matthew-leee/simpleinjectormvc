﻿using BLL.Contract.Commands.UserCommands;
using BLL.Contract.Models.User;
using BLL.Contract.Queries.UserQueries;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Contract.Interfaces.IRepositories
{
    public interface IUserRepository
    {
        void RegisterUser(RegisterUserCommand registerUserCommand);
        IList<ValidateRegisterUser> ValidateRegisterUser(ValidateRegisterUserQuery validateRegisterUserQuery);
        LoginUserPassword LoginUser(LoginUserSingleQuery query);
    }
}
