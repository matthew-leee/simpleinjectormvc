﻿using BLL.Contract.Commands.UserCommands;
using BLL.Contract.Models.User;
using BLL.Contract.Queries.UserQueries;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Contract.Interfaces.IRepositories
{
    public interface IAsyncUserRepository
    {
        Task RegisterUser(RegisterUserCommand registerUserCommand);
        Task<IList<ValidateRegisterUser>> ValidateRegisterUser(ValidateRegisterUserQuery validateRegisterUserQuery);
        Task<LoginUserPassword> LoginUser(LoginUserSingleQuery query);
    }
}
