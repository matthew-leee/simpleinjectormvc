﻿using BLL.Contract.Commands.OrderCommands;
using BLL.Contract.Models.Order;
using BLL.Contract.Queries.OrderQueries;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Contract.Interfaces.IRepositories
{
    public interface IOrderRepository
    {
        void CreateOrder(CreateOrderCommand command);
        List<ViewOrder> ViewOrder(ViewOrderQuery query);
    }
}
