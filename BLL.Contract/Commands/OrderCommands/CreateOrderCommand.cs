﻿using BLL.Contract.Models.Order;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Commands;

namespace BLL.Contract.Commands.OrderCommands
{
    public class CreateOrderCommand : ICommand
    {
        public CreateOrder CreateOrder { get; set; }
    }
}
