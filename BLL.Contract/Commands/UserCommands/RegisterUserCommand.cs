﻿using BLL.Contract.Models.User;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Commands;

namespace BLL.Contract.Commands.UserCommands
{
    public class RegisterUserCommand: ICommand
    {
        public RegisterUserCommand (RegisterUser registerUser)
        {
            this.RegisterUser = registerUser;
        }
        public RegisterUser RegisterUser { get; set; }
    }
}
