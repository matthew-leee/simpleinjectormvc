﻿using BLL.Contract.Models.Order;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Queries;

namespace BLL.Contract.Queries.OrderQueries
{
    public class ViewOrderQuery: IQuery<ViewOrder>
    {
        public FilterOrder filterOrder;
        public SortOrder sortOrder;
        public ViewOrderQuery(FilterOrder filterOrder, SortOrder sortOrder)
        {
            this.filterOrder = filterOrder;
            this.sortOrder = sortOrder;
        }
    }
}
