﻿using BLL.Contract.Models.User;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Queries;

namespace BLL.Contract.Queries.UserQueries
{
    public class LoginUserSingleQuery: ISingleQuery<LoginUserPassword>
    {
        public string LoginUserInputPassword;
        public LoginUser LoginUser;
        public LoginUserSingleQuery(LoginUser loginUser)
        {
            LoginUser = loginUser;
            LoginUserInputPassword = loginUser.Password;
        }
    }
}
