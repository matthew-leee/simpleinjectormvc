﻿using BLL.Contract.Models.User;
using System;
using System.Collections.Generic;
using System.Text;
using TVDMS.Common.Queries;

namespace BLL.Contract.Queries.UserQueries
{
    public class ValidateRegisterUserQuery: IQuery<ValidateRegisterUser>
    {
        public ValidateRegisterUser ValidateRegisterUser;
        public ValidateRegisterUserQuery(ValidateRegisterUser validateRegisterUser)
        {
            ValidateRegisterUser = validateRegisterUser;
        }
    }
}
